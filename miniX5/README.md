# MiniX5: A generative program
# ReadMe
Hej og velkommen til min miniX5

# RunMe link
En lille smagsprøve på min miniX5 ”A generative program”, så klik her for at se hvad jeg har lavet juhu

[Mit program](https://theauhd.gitlab.io/aestetiskprogrammering/miniX5/index.html)

[Min kode](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX5/sketchminix5.js) 

**Billeder af mit program:**
 ![første billede](miniX5/minix5billede.png) 

**Hvad har jeg programmeret?**

Jeg har programmeret et generativt program med trekanter. Trekanterne har alle et fast punkt, hvor de hænger sammen. De to andre punkter afhænger både af framecounten og et random tal indenfor bredden. Derudover har trekanterne alle random farver. 

**Regler for mit generative program:**

1.	Hver gang siden bliver refreshed skifter trekanterne mønster.
2.	Hver gang skifter trekanterne størrelse. 
3.	Jo længere tid man er inde på programmet, jo længere ned bevæger trekanterne sig.
4.	Trekanterne får nye random farver hver gang programmet starter.

Jeg har defineret x til at være lig med bredden, og i For-loopet har jeg gjort sådan at variablen y følger med Framecounten. Dette gør at trekanterne har mulighed for at bevæge sig længere og længere ned, jo længere tid programmet kører. 

Det er altså ikke et resultat i dette program, da det bliver ved med at køre og er ikke kodet til at stoppe på noget tidspunkt, hvis der ikke bliver interageret med det. Det er altså brugeren der bestemmer, hvornår de har set nok, og refresher siden, som får koden til at starte forfra. 


**Hvad var svært:**

Jeg har kun benyttet mig af et enkelt for-loop, ellers har jeg mest brugt random funktionen til at gøre det generativt, og forskelligt hver gang. Dette skyldes, at jeg virkelig synes det var svært at for de her for-loops til at virke som jeg gerne ville. Jeg havde mange forskellige ideer til hvordan jeg ville lave det her program, men som alle gik i vasken på grund af det her skide for-loop. Hvilket var derfor jeg valgte at gå med trekanterne her. Jeg tror jeg har lidt svært ved at forstå hvordan man kan bruge de for-loops, så det fungerer som jeg gerne vil have det. Så det er noget jeg vil prøve at øve mig på. 
På et tidspunkt besluttede koden sig også for at lave flere trekanter på samme tid, hvilket jeg ikke kunne finde ud af at fikse, så jeg kørte bare med den.

**Mine tanker om temaet ”A generative program”**

Da jeg overvejede dette tema, tænkte jeg på hvad der egentlig var tiltrækkende ved disse slags programmer, som der har en stor randomfaktor. Her kom jeg frem til noget, som jeg faktisk ikke selv har fået vist så godt i min kode, men som jeg gerne ville have gjort mere af. Nemlig det med at man kan se når der sker noget nyt, som ikke skete før, altså det uforudsigelige. Det er det uforudsigelige, som gør disse koder til noget anderledes end andre. I kapitlet ”Randomness” (Montfort, 2012), er det også noget af det han siger er det gode med tilfældigheder i spil: 
”Once the outcome of a game is known, the game becomes meaningless”. Jeg mener det er det samme i de her generative programmer. Måske er det sammenhængen med livet, og hvordan det også afhænger af tilfældigheder, og man ved aldrig hvad det næste der sker er, eller måske er det for dybt. Dette er noget jeg måske godt gad at have udforsket noget mere i at gøre synligt. 
Selvom en generativ kode ikke skal have et random element, kan det visuelle stadig godt ligne at det har, hvilket får det til at se spændende ud. 





