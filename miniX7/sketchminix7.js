fishSize = {
  w:190, h:120
};

let fishpic;
let baggrund;
let score =0;
let minshark = 4;
let minfood = 4;
let sharkpic;
let food;
let foodpic;
let shark = [];
let foodarray = [];
let fishPosY;
let fishPosX =120;
let eatSound;
let song;
let start;
let deadSound;

//preloader billederne som jeg skal bruge
function preload(){
  fishpic = loadImage('giphy.gif');
  baggrund = loadImage('baggrund1.jpg');
  sharkpic = loadImage('shark.gif');
  foodpic = loadImage('seaweed.png');
  soundFormats('mp3');
  eatSound = loadSound('chew.mp3');
  song = loadSound ('song.mp3');
  deadSound = loadSound ('deadsound.mp3');
}

function setup() {
createCanvas(windowWidth, windowHeight);

//start knappen
knap6 = createButton("START SPIL");
knap6.position(width/2-50,500);
knap6.size(200, 50);
knap6.style("background-color", "#00ffFF");
knap6.style("color", "#000000");

knap6.mousePressed(() => start = true);


}
//når musen trykkes, starter sangen 
function mousePressed() {
  if (song.isPlaying()) {
    // .isPlaying() returns a boolean
    song.stop();
   
  } else {
    song.play();
    
  }
}

function draw() {
//baggrundsbilledet
image(baggrund,0,0,windowWidth, windowHeight);
//hvis start knappen trykkes skal funktionen startspil starte
if(start===true){
  knap6.hide();
  image(baggrund,0,0,windowWidth, windowHeight);
  startSpil(); 

}}

function startSpil(){

displayScore();
moreSharks();
checkSharkNum();
showShark();
showFood();
checkFoodNum();

//fisken der svømmer, følger musen på y-aksen
push();
fishPosY = constrain(mouseY, 0, height);
imageMode(CENTER);
image(fishpic, fishPosX, fishPosY, fishSize.w, fishSize.h);
pop();
checkpoint()

gameover();
}



function checkSharkNum(){ //Gør sådan at der bliver ved med at komme flere hajer
 if(shark.length < minshark) {
   shark.push(new Shark());
 }

}

function showShark(){ //viser hajens class
  for (let i = 0; i <shark.length; i++) {
    shark[i].show();
    shark[i].move();
   
   
  }}

  
  

  function checkFoodNum() { //Gør sådan at der bliver ved med at komme mere mad
    if (foodarray.length < minfood) {
      foodarray.push(new Food);
    }
  }
  
  function showFood(){//viser madens class
    for (let i = 0; i <foodarray.length; i++) {
      foodarray[i].show();
      foodarray[i].move();
     
    }
  }  
  function displayScore() { //viser scoren og teksten omkring hvad man skal
    fill(0,0, 240);
    textSize(20);
    textFont("Staatliches");
    text('YOUR SCORE: ' + score , width/2-50,100);
    fill(0,0,240);
    text('EAT THE SEAWEED, AVOID THE SHARKS', width/2-120,50);
}
function checkpoint() { //gør sådan at scoren går op når fisken rammer maden
  let distance;
  for (let i = 0; i < foodarray.length; i++) {
      distance = dist(fishPosX, fishPosY, foodarray[i].pos.x, foodarray[i].pos.y);

      if (distance < fishSize.w/2) {
          eatSound.play();
          score++; 
          foodarray.splice(i,1);
          
      
      }}}

function moreSharks(){ //gør sådan at der kommere 6 hajer istedet for 4 når ens score bliver højere end 8
  if (score > 8) {
    minshark=6;
    minfood=5;

  }}

function gameover() { //gameover når man rammer en haj
    let distance2;
       for (let i = 0; i < shark.length; i++) {
        distance2 = dist(fishPosX, fishPosY, shark[i].pos.x, shark[i].pos.y);
      
         if (distance2 < fishSize.w/2) {
            shark.splice(i,1);
            deadSound.play();
            noLoop();
            textSize(45);
            textFont("Staatliches");
            text("GAME OVER!",width/2-60,height/2);; 
            text('YOUR SCORE: ' + score , width/2-75,height/2+50);
            textSize(20);
            fill(0);
            text('YOUR SCORE: ' + score , width/2-50,100);
            text('EAT THE SEAWEED, AVOID THE SHARKS', width/2-120,50);
            
           //Her laves der en knap, så kan være med til at reloade siden, så man kan prøve igen
        button = createButton('TRY AGAIN');
        button.position(width/2-120, 500);
        button.style("background-color", "#000fFF")
        button.size(300, 50);
        button.style("color", "#000000");
        button.size('back');
        button.mousePressed(tryAgain);

          }}}
          function tryAgain() {
            window.location.reload(true);
          }
    