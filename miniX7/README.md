# MiniX7: Genbesøg en miniX 
## ReadMe
Hej og velkommen til min miniX7

## RunMe link
En lille smagsprøve på min miniX7 ”Genbesøg en miniX”, så klik her for at se hvad jeg har lavet juhu

[Mit program](https://theauhd.gitlab.io/aestetiskprogrammering/miniX7/index.html)

[Min kode](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX7/sketchminix7.js)

[Food.js](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX7/Food.js)

[Shark.js](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX7/Shark.js)
**Billeder af mit program:**
 ![første billede](miniX7/mitspil2.png) ![andet billede](miniX7/mitspil1.png) ![tredje billede](miniX7/mitspil3.png) 


### Forbedringer til næste spil
Jeg har valgt at forbedre min miniX6, altså mit spil, da jeg syntes det godt kunne bruge lidt forbedringer, og jeg synes det var noget af det der var sjovest at ændre på. Nogle af de ting jeg har ændret på, er noget som vi har lært gennem hele forløbet, og ikke bare noget af det nye. 

De ændringer jeg har lavet, går blandt andet ud på at jeg ville gøre spillet lidt mere spændende, end bare at flytte en fisk op og ned, uden at ændre på de regler jeg havde lavet som gælder for spillet. Det har jeg prøvet at gøre ved bl.a. at tilføje noget baggrundsmusik, bare for sjov, men også fordi det gør det lidt mere spændene. Derudover har jeg også tilføjet lydeffekter, når fisken spiser noget, samt når man taber og bliver spist af en haj. Sangen spiller når man trykker med musen, og kan derfor også slåes fra hvis man trykker med musen igen. Efter jeg lavede disse lydeffekter og satte lyden ind, har jeg fået meget mere styr på hvordan man bruger det, da jeg ikke havde prøvet det før i de andre miniX’er. Jeg havde derfor også lidt problemer med det, men det har bare gjort nu at jeg har fået endnu mere styr på det.


For at få musikken til at starte, uden man tænkte over at man starter den selv, lavede jeg derudover en start knap. Jeg tænkte at det ellers er det ikke sikkert man havde trykket med musen, og derfor ikke vidst at der var musik. 

Til sidst har jeg justeret lidt på min gameover skærm, gjort den lidt pænere, samt tilføjet en retry knap, som gør at siden man spiller på vil blive genindlæst, så man kan prøve igen. Disse ændringer har jeg lavet, sådan at spillet ville blive mere flydende og føles lidt mere som en ægte spil. 


### Overvejelser omkring æstetisk programmering
Noget af det vi virkelig også lærer når man laver sådan en øvelse, er også at stille spørgsmålstegn til noget af de eksisterende teknologier der er og det vi selv programmerer. Her kan vi virkelig engagere sig i programmeringen og skabe ændringer, som gør det vi har lavet bedre. De små øvelser, gir os også et indblik i hvordan programmering er i det store billede. Programmering er en nemlig en dynamisk kulturel praktis, som hele tiden ændrer sig, og som man hele tiden kan se på med nye kritiske øjne. Det kan hjælpe os til tænke anderledes og forstå nogle af de komplekse procedurer, som udgør vores realiter i verden. 
Så små opgaver som denne kan være hjælpsomme, når man skal tænke kritisk på digitale designs. 

> “We follow the principle that the growing importance of software requires a new kind of cultural thinking, and curriculum, that can account for, and with which to understand better, from within, the politics and aesthetics of algorithmic procedures, data processing, and abstracted modeling” 

Det er noget de også beskriver i bogen omkring æstetisk programmering, at software og dermed programmering altid ændre sig, og man skal derfor altid være kritisk, både overfor det nye, men også det gamle. 




