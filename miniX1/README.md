# miniX1 README

Hej og velkommen til min første README 

## RunMe Link:
https://theauhd.gitlab.io/aestetiskprogrammering/miniX1/index.html 

En lille smagsprøve på mit aller første programmerings arbejde (nogensinde) Hold da op.

## README 

**Hvad har jeg produceret?**

Jeg har med min kode produceret en det der skal ligne Jorden, der cirkulerer omkring solen. Imens jorden cirkulerer rundt om solen, drejer jorden selv rundt om sig selv. Derudover er der en lille måne som drejer med rundt om jorden. Derudover har jeg lavet en mørkeblå baggrund, som skal ligne nattehimlen. Jeg fik ideen til at lave dette, da jeg legede lidt rundt med de forskellige funktioner der findes inde på referencer på p5.js, hvor jeg lavede en kugle der kunne dreje rundt. Herefter legede jeg lidt med at sætte flere planeter ind på, men da jeg havde problemer med at flytte koordinaterne til kuglerne, valgte jeg at droppe det. I stedet valgte jeg at tilføje månen, da det var nemmere når de skulle dreje omkring samme akse. Solen tilføjede jeg til sidst, så det lignede at man kiggede på jorden fra solens perspektiv. 
 
Jeg synes det var sjovt at lege lidt med hvad man kunne opfinde i 3D ved hjælp af koden, men der var dog problemer, som gjorde det lidt svære at kode i 3D. Derfor vil jeg nok vælge at kode noget i 2D næste gang, for at få en bedre forståelse for det basic. F.eks. havde jeg problemer med at skrive en tekst inde i min RunMe. Jeg afprøvede i en anden sketch, uden 3D, for at tjekke om det var koden der var noget galt med, hvilket det ikke var. Desuden havde jeg som skrevet tidligere problemer med at rykke 3D figurerne rundt på lærredet. Jeg tænker og håber, at vi i løbet af semesteret, lærer noget mere om dette, da jeg synes det virkelig er spændende. 

Selvom jeg synes programmeringen er sjov, kan jeg godt mærke, at man kan virkelig meget med det, som jeg har slet ingen forståelse for endnu. Hvilket jeg selvfølgelig håber jeg kommer til. Det er nu ma begynder at se hvor meget der er og hvor komplekst det virkelig er indenunder det, vi ser på en skærm.

**Hvad betyder min kode?**

Jeg startede med at oprette et lærred, som skulle være størrelsen på det vindue, som man åbner koden i. Desuden tilføjede jeg WEBGL til sidst, for at jeg kunne arbejde med 3D objekter. 

Under `funktion draw()`, starter der med at være en baggrund, med en mørkeblå farvekode. Grunden til at koden til baggrunden er placeret i `funktion draw()` er så der ikke kommer et spor fra kuglen i dens retning, som der ville hvis baggrunden ikke blev tegnet efter hver gang kuglen har flyttet sig. 

Herefter kommer koden for solen, som er en ellipse, der er placeret helt i bunden af mit lærred. Den har jeg farvet med en hvid farve kode, og fjernet dens omkredsning. 

Da jeg så skulle lave det der skal forstille sig jorden, lavede jeg det der hedder en ellipsoid, og skrevet den størrelse i koordinaterne bagved. Denne har jeg så givet en blå farve og grønt mønster. For at få den til at køre rundt om sig selv, har jeg brugt funktionen `rotateY(frameCount*0,1)`. Dette gør at kuglen roterer omkring y-aksen, og tallet afgør hastigheden. 

Funktionen `translate(width / 1.5, height / 4)`, får kuglen til at køre rundt om y-aksen, på x-aksens flade (man kan tilføje et ekstra koordinat, hvis den skal bevæge sig i z-aksens koordinater også), som ses tydeligere når det er i 3D. tallet 1.5 afgøre hvor langt kuglen skal komme ud. Tallet 4 angiver hvor på y-aksen den skal befinde sig.


Til sidst har til tilføjet månen, som altså er en lille ellipsiod. Denne har jeg givet en hvid/grå farvetone. Da denne drejer sig om den samme akse, som ”Jorden” gør, følger månen dens bevægelse. Jeg har tilføjet både 
`rotateY(frameCount * 0.03)` og 
`rotateX(frameCount * 0.03)`
så månen bade roterer omkring y og x-aksen og hvilket dermed skaber en mere realistisk syn på hvor månen kan befinde sig ud fra jordens rotation. Dertil har jeg også sat hastigheden lidt op, for månens rotation rundt om jorden lidt op. Da denne måne også skulle rotere med rundt om solen, har jeg endnu engang brugt funktionen `translate(width / 4, height / 5)`. Jeg har dog bare sat lidt op, så den ikke roterer i så stor en omkreds (Jo mindre tallet er jo større en omkreds cirkulerer den i, men har ikke helt kunne finde ud af hvorfor). 


Det var vist det for min kode, her til første miniX. Jeg har prøvet at bruge lidt forskellige funktioner, som jeg syntes virkede interessant. Der var dog flere, som ikke kom med i mit endelige produkt, da jeg ikke synes det passede ind, eller hvis det ikke endte som jeg ville have det, hvilket jeg vil arbejde med i næste opgave. 





