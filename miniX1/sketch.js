function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight,WEBGL);
  

}
function draw() {
  // put drawing code here
  background(25, 25, 112,250);

  fill(255,215, 0)
  noStroke();
  ellipse(0,410,800,300)


  rotateY(frameCount * 0.01); //får kuglen til at rotere omkring y-aksen
  translate(width / 1.5, height / 4); 

  stroke(34,139, 34);
  strokeWeight(10);
  fill(100,149,237)
  ellipsoid(200, 200, 200);

  
  rotateY(frameCount * 0.03);
  rotateX(frameCount * 0.03);
  translate(width / 3, height / 5);
  stroke(255,255,224);
  ellipsoid(40, 40, 40);

}
