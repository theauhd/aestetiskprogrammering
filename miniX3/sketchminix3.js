
//Min Throbber

//variabler til at få dem til at dreje rundt
let dreje = 0;
let dreje2 = 0;

function setup() {
  // put setup code here
createCanvas(windowWidth,windowHeight); 
//Jeg laver vinkel mode om til grader i stedet for radianer
angleMode(DEGREES);
  
}

function draw() {
  // put drawing code here
  background(0);  

 //Få mine tandhjul til at dreje rundt 
push();
  translate(width/2,height/2); //Laver koordinatsystemmets 0,0 punkt om 
  rotate(dreje); //den skal rotere med min variable
  tandhjul(0,0,45,41,75);
  dreje=dreje+1
pop();

 //Få mine tandhjul til at dreje rundt 
push();
  translate(width/2+80,height/2+80) ////Laver koordinatsystemmets 0,0 punkt om 
  rotate(dreje2);
  tandhjul(0,0,59,55,100);
  dreje2=dreje2-0.8;
pop();

push();
  translate(width/2-20,height/2+85) ////Laver koordinatsystemmets 0,0 punkt om 
  rotate(dreje2);
  tandhjul(0,0,32,28,50);
  dreje2=dreje2-0.6;
pop();

//tekst
fill(100);
textSize(20);
text("LOADING...",width/2-20,height/2+200);

//Jeg laver en funktion så jeg kan få flere tandhjul i forskellige størrelser
  function tandhjul (x,y,dut,dut2,diameter){
 //De små firkanter rundt om cirklen
  push();
  fill(100);
  noStroke();
  rectMode(CENTER);
  rect(x,y+dut,19,9)
  rect(x,y-dut,19,9)
  rect(x+dut,y,9,19)
  rect(x-dut,y,9,19)
pop();

push();
fill(100);  
rectMode(CENTER);
  translate(x,y);
  rotate(45);
  rect(0,dut2,19,19);
pop();


push();
fill(100);  
rectMode(CENTER);
  translate(x,y);
  rotate(135);
  rect(0,dut2,19,19);
pop();
  
push();
fill(100);  
rectMode(CENTER);
  translate(x,y);
  rotate(45);
  rect(dut2,0,19,19);
pop();

push();
fill(100);  
rectMode(CENTER);
  translate(x,y);
  rotate(135);
  rect(-dut2,0,19,19);
pop();

//Cirklen
  ellipseMode(CENTER);
  stroke(100);
  strokeWeight(10);
  noFill();
  ellipse(x,y,diameter);
}
}