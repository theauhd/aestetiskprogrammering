# MiniX3 ReadMe

Hej og velkommen til min miniX3

# RunMe link
En lille smagsprøve på min miniX3 throbber, så klik her for at se hvad jeg har lavet juhu

![Et screenshot](miniX3/billedeminix3.png) 

[Mit program](https://theauhd.gitlab.io/aestetiskprogrammering/miniX3/index.html)

[Min kode](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX3/sketchminix3.js) 

**Hvad har jeg programmeret?**

Jeg har lavet mig selv en lille throbber. Throbberen består af tre tandhjul i hver sin størrelse, som drejer rundt. Under i en lille tekst står der ”LOADING…” for at vise at throbberen indikerer at computeren loader.

**Nye funktioner jeg har brugt:**
-	Jeg har lavet mine egne funktioner, med forskellige variabler. Dette gjorde jeg, så jeg kunne lave flere tandhjul i forskellige størrelser, uden at skulle skrive hele koden flere gange. Det synes jeg var meget fedt, og brugbart når man skal lave den samme 
avancerede figur flere gange. 

-	`translate()` -> som rykkede koordinatsystemets 0-punkt til et nyt sted på canvas. Jeg brugte den ved hvert tandhjul, og satte den derfor ind i `push()` og `pop()` funktionen så det ikke rodede rundt med de andre tandhjul. 
-	`rotate()` -> For at få tandhjulene brugte jeg denne sammen med de variabler jeg havde lavet, det fungerede også meget godt for mig synes jeg. Sjovt at lave noget der bevæger sig. 
-	`angleMode()` -> `angleMode(DEGREES)` brugte jeg i starten, så jeg kunne skrive vinklerne i grader i stedet for radianer, da jeg følte det var nemmere. 

**Hvad var svært:**

Det var svært at få de små firkanter til at passe på cirklen fordi de skulle drejes, og passe med canvas, og jeg forestiller mig der var en nemmere måde, men den fandt jeg aldrig. 
Jeg gad godt at de små tandhjul passede ind i hullerne med hinanden, men kunne jeg heller ikke få dem til, selvom jeg legede lidt med hastigheden de skulle dreje på, passede de stadig ikke hver gang. 

**Mine tanker om temporalitet og throbbers**

Som vi snakkede om i timen, er maskinen og menneskets forståelse af tid meget forskelligt. “_Machine-time operates at a different register from human-time, further complicated by global network infrastructures, and notions of real-time computation_” (Soon & Cox). Hos mennesker er tid ofte meget subjektivt, og meget omkring hvor lang tid det føltes, og ikke hvor lang tid noget rent faktisk varer. Dette koncept er også noget som en throbber kan få frem i personer. Hvis man ser sådan en throbber i mere end et par sekunder, kan vi tit begyndte at føle at det tager ”lang tid”, selvom det i virkeligheden ikke gør. Et citat som beskriver dette; “_we are shifting our attention from the past to the present, and our “now” is getting shorter_” (The Philosophy of Software, kap. 6 Real-time streams – David Berry 2011). Vores tålmodighed og hvor meget tid vi regner med at maskinen skal bruge bliver kortere og kortere

En anden følelse en throbber ofte giver er at noget er i stykker eller gået galt. Det er også derfor jeg har lavet min som jeg har gjort. Fordi dette er sådan jeg ofte føler. Noget er gået i hak, eller ikke virker, så jeg må hellere reloade, er altid min første tanke. Derfor kom jeg til at tænke på de her tandhjul, og hvordan det jo bare er en maskine, og som skal være sat sammen på den rigtige måde, og alt skal passe før det virker optimalt, og der kan derfor godt gå noget galt. Ligesom Friedrich Kittler sagde ”_Der er software – alt er hardware! og hardware er forgængeligt_”. I min forestilling, vil en throbber altså køre for evigt til dette hardware er død. 

Noget andet vi snakkede om i timen, var det her med at der var blevet programmeret falske forsinkelser i nogle programmer, så de virkede mere troværdige. Hvilket er noget jeg også synes er sjovt at tænke på, fordi man føler virkelig at det er fordi maskinen tænker, så når der er en kort forsinkelse, føler man at computeren har tænkt sig bedre om end hvis den ikke havde denne korte forsinkelse. Synes bare det er sjovt, hvordan vi tit putter den menneskelige tilgang til ting over på en maskine, selvom det jo bare er en maskine der passer sig selv. Maskiner får generelt også bare flere og flere sådan menneskelige kvaliteter, især også i forhold til tid, som f.eks. nattetilstand. 


