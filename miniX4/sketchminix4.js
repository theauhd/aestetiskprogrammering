//miniX4
let bgColor = '#72B8CA' //baggrundsfarven så billedet også bliver farvet
let capture;
let eye;
let knap;
let skriv;

let minx;
let maxx;
let miny;
let maxy;

function setup() {
  createCanvas(900,600);
  
minx=270;
maxx=550;
miny=300;
maxy=430;

eye=loadImage('eyes6.png'); //billedet af øjet

capture =createCapture(VIDEO); //Tildele video input objektet
capture.size(900,480); //Bestemme størrelsen med
capture.hide(); //gem webcam feed’et


skriv = createInput("?");
skriv.size(240)
skriv.position(80, 550);

}

function draw() {
  // put drawing code here
 
//baggrundsfarve
background(bgColor)

//det hvide inden i øjet
fill(240);
noStroke();
rect(200,200,500,300);

//variabler som gør at pupillen bliver inde i øjets
let pupil = constrain(mouseX, minx, maxx);
let pupiller = constrain(mouseY,miny,maxy);
//videoen, som er blevet mindre
push();
imageMode(CENTER);
image(capture,pupil,pupiller,160,150);

pop();

//Det sorte inden i øjet

noFill();
strokeWeight(20);
stroke(0);
ellipse(pupil,pupiller,180,170);



//Det grønne/mønster inden i øjet 
noFill();
strokeWeight(40);
stroke(73,166,110);
ellipse(pupil,pupiller,210,200);

noFill();
strokeWeight(7);
stroke(50,153,89);
ellipse(pupil,pupiller,200,190);

noFill();
strokeWeight(7);
stroke(40,143,86);
ellipse(pupil,pupiller,225,217);

noFill();
strokeWeight(7);
stroke(26,128,67);
ellipse(pupil,pupiller,250,240);

noFill();
strokeWeight(3);
stroke(0);
ellipse(pupil,pupiller,255,245);

fill(255);
strokeWeight(1);
ellipse(pupil+60,pupiller-50,30);


//Her har gjort billedet af øjet grønt og fået det til at passe på canvas
push();
imageMode(CENTER);
tint(bgColor);
image(eye,430,300,900,600);
pop();

//Teksterne
fill(0);
textSize(40);
textFont("Bungee Inline");
strokeWeight(3);
stroke(0);
text("AI IS WATCHING YOUR NEXT MOVE", 110,160);

textSize(20);
text("W H O  A R E  Y O U ?",70,540);


//Invert filter hvis man trykker på en knap
if(keyIsPressed){
 filter(INVERT);  

  }


}

  
  
