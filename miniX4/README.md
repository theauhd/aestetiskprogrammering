# MiniX4 ReadMe
Hej og velkommen til min miniX4

# RunMe link
En lille smagsprøve på min miniX4 ”Capture all”, så klik her for at se hvad jeg har lavet juhu

[Mit program](https://theauhd.gitlab.io/aestetiskprogrammering/miniX4/index.html)

[Min kode](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX4/sketchminix4.js)

**Billeder af mit program:**
 ![første billede](miniX4/billede1.png) 

![andet billede](miniX4/billede2.png) 

**Title og beskrivelse:**

Jeg har valgt at kalde mit program ”The Eye”. Dette har jeg valgt pga. hele den her debat omkring hvorvidt dette data som hele tiden bliver indsamlet omkring os, kan forståes som en form for overvågning. Det er ligesom Big Brother konceptet, som der lige pludselig kan komme til virkelighed, nu når det er blevet så nemt at indsamle diverse data om folk. Dette kan virke rigtig skræmmende, hvilket også er det jeg viser her med mit program. Det er bare den verden vi lever i nu, og selvom det kan være skræmmende, må vi også bare omfavne den, for denne datafication, bliver nok bare større med tiden. 

**Hvad har jeg programmeret?**

Når man kommer ind på mit program, er det første man vil se et stort øje, som kigger på dig igennem dit kamera. Videoen af dig selv er placeret inde i pupillen på øjet, og vil flytte sig efter din mus, så det giver effekten af at du bliver overvåget at dette øje. Øverst på canvas har jeg indsat en tekst ”AI is watching your next move”. I bunden er der så en bar, hvor du kan skrive hvem du er. Når man så begynder at skrive, vil der komme et Invert filter på, hver gang en knap bliver trykket ned på, for at gøre det hele lidt mere dramatisk. 
Jeg har forsøgt at få data capture ind i mit program, ved hjælp af mousetracking, video og keyboardet. 

**Nye funktioner jeg har brugt:**
-	Mange af de nye funktioner jeg har brugt, har været i relation til at få videoen fra kameraet frem på mit canvas. Her har jeg som vi lærte i klassen brugt variabler til at lave denne video, med `createCapture(VIDEO)`.
-	`createInput(””)` -> denne har jeg brugt til at lave baren som man kan skrive hvem man er i.
-	`constrain()` -> har jeg brugt, så pupillerne ikke forsvandt ud af øjet med musen, men blev inden for et bestemt rum. 
-	`if (keyIsPressed) `-> til sidst har jeg brugt denne, så filteret ændrede sig når en tast blev trykket ned. 

**Hvad var svært:**

Jeg synes det var lidt svært, at få baggrunden (billedet af øjet), til at ændre farve uden at ændre det hele. Det skal siges at baggrunden er et billede jeg har fundet, som var hvid, så jeg skulle ændre billedet farve, hvilket jeg havde lidt problemer med, men fandt ud af til sidst. Derudover havde jeg også nogle problemer med at få pupillerne til at blive indeni øjet, og ikke bare følge musen, men da jeg fandt ud af man kunne bruge `constrain()`, virkede det. 

**Mine tanker om temaet ”Capture All”**

I timen snakkede vi om, hvordan man kan se "datafikationsprocesser som en ny måde at fortolke verden på", om hvordan Shoshana Zuboff, mener at vi lever i såkaldt Surveillance capitalism, hvor alt vores menneskelig erfaring bliver råmaterialet, der producerer de adfærdsdata, der bruges til at påvirke og endda forudsige vores handlinger, kun til kapitalistiske formål. Det er helt vanvittigt, hvad noget data er hver af penge, og hvorfor? Hvorfor er data blevet så meget værd i samfundet? Altså det er jo svært at svare på, men jeg synes bare det er spændende, noget som mange af os måske ikke engang tænker over, f.eks. som at trykke ja til cookies, søge på google eller indtaste personlige information på hjemmesider der spørger, er så meget værd. Nu når vi har om dette tema, kan jeg dog ikke lade hver med at have en følelse af at alt hvad man gør på nettet, bliver overvåget, og engang kan bruges i mod en, og det er faktisk bare det jeg har prøvet at vise med mit program.  


