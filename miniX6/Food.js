

class Food {
  constructor() { //initalize the objects
    this.speed = floor(random(2, 6));
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.sizey = floor(random(50, 70));
    this.sizex = floor(random(120, 150));

  }
  move() {  //moving behaviors
    this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
    if (this.pos.x<-60){//hvis tangens position kommer ud af skærmen skal der komme en ny et random sted
      this.pos.x=width+5;
      this.pos.y=random(height);
  }
  }
  show() { //show tofu as a cube
    image(foodpic,this.pos.x,this.pos.y,this.sizex,this.sizey);
   


  }
}