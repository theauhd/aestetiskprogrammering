fishSize = {
  w:190, h:120
};

let fishpic;
let baggrund;
let score =0;
let minshark = 4;
let minfood = 4;
let sharkpic;
let food;
let foodpic;
let shark = [];
let foodarray = [];
let fishPosY;
let fishPosX =120;

//preloader billederne som jeg skal bruge
function preload(){
  fishpic = loadImage('giphy.gif');
  baggrund = loadImage('baggrund1.jpg');
  sharkpic = loadImage('shark.gif');
  foodpic = loadImage('seaweed.png');

}

function setup() {
createCanvas(windowWidth, windowHeight);
  

}
function draw() {
//baggrundsbilledet
background(30,100,255);
image(baggrund,0,0,windowWidth, windowHeight);

displayScore();
moreSharks();
checkSharkNum();
showShark();
showFood();
checkFoodNum();

//fisken der svømmer, følger musen på y-aksen
push();
fishPosY = constrain(mouseY, 0, height);
imageMode(CENTER);
image(fishpic, fishPosX, fishPosY, fishSize.w, fishSize.h);
pop();
checkpoint()

gameover();

}
function checkSharkNum(){ //Gør sådan at der bliver ved med at komme flere hajer
 if(shark.length < minshark) {
   shark.push(new Shark());
 }

}

function showShark(){ //viser hajens class
  for (let i = 0; i <shark.length; i++) {
    shark[i].show();
    shark[i].move();
   
   
  }}


  function checkFoodNum() { //Gør sådan at der bliver ved med at komme mere mad
    if (foodarray.length < minfood) {
      foodarray.push(new Food);
    }
  }
  
  function showFood(){//viser madens class
    for (let i = 0; i <foodarray.length; i++) {
      foodarray[i].show();
      foodarray[i].move();
     
    }
  }  
  function displayScore() { //viser scoren og teksten omkring hvad man skal
    fill(0,0, 240);
    textSize(17);
    text('YOUR SCORE: ' + score , width/2-50,100);
    fill(0,0,240);
    text('EAT THE SEAWEED, AVOID THE SHARKS', width/2-150,50);
}
function checkpoint() { //gør sådan at scoren går op når fisken rammer maden
  let distance;
  for (let i = 0; i < foodarray.length; i++) {
      distance = dist(fishPosX, fishPosY, foodarray[i].pos.x, foodarray[i].pos.y);

      if (distance < fishSize.w/2) {
          foodarray.splice(i,1);
          score++; 
      
      }}}

function moreSharks(){ //gør sådan at der kommere 6 hajer istedet for 4 når ens score bliver højere end 8
  if (score > 8) {
    minshark=6;
    minfood=5;

  }}

function gameover() { //gameover når man rammer en haj
    let distance2;
       for (let i = 0; i < shark.length; i++) {
        distance2 = dist(fishPosX, fishPosY, shark[i].pos.x, shark[i].pos.y);
      
         if (distance2 < fishSize.w/2) {
            shark.splice(i,1);
            noLoop();
            textSize(25);
            text("GAME OVER!",width/2-50,height/2);; 
            text('YOUR SCORE: ' + score , width/2-65,height/2+50);
            textSize(17);
            fill(0);
            text('YOUR SCORE: ' + score , width/2-50,100);
            text('EAT THE SEAWEED, AVOID THE SHARKS', width/2-150,50);
            }}}
    