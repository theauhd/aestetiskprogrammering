# MiniX6: Games with objects

## RunMe link
En lille smagsprøve på min miniX6 ”Games with objects”, så klik her for at se hvad jeg har lavet juhu

[Mit program](https://theauhd.gitlab.io/aestetiskprogrammering/miniX6/index.html)

[Min kode](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX6/sketchminix6.js) [Class food](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX6/Food.js) [Class shark](https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX6/Shark.js)


**Billeder af mit program:**
 ![første billede](miniX6/mitspil.png) 


## ReadMe
Hej og velkommen til min miniX6

### Hvad har jeg programmeret?

Spillet som jeg har programmeret, har jeg fundet på med inspiration fra tofu-spillet som vi har haft om i klassen. Det er lidt det samme princip, dog har jeg lavet mit eget spin på det, og tilføjet nogle funktioner. 
Spillet går ud på at fisken skal spise tang, og hvert stykke tang man spiser, giver et point. Ud over dette skal man undvige hajerne. Hvis man rammer en haj, er spillet slut, og den endelige score bliver vist. For at gøre spillet lidt svære, har jeg tilføjet en funktion som gør at når ens score kommer over 8, vil der komme flere hajer, som skal undviges. 


### My programs objects and their related attributes, and the methods in my game

I mit spil, har jeg tilføjet fisken, som følger musen på y-asken, men har en fast plads på x-aksen. Så man kan bevæge fisken ved hjælp af musen. 
I opgaven skulle man lave mindst en ”class-based object-oriented apoach” og dertil have ”at least have a class, a constructor, and a method”. Dem har jeg lavet to af, en til hajerne, og en til tangen. 

`class Shark {` består af en contructor først, i denne beskriver jeg dens properties, herindunder dens speed, size, og position. 
Bagefter kommer dens methods. Herindunder en `show()`, og en `move()`. Under `show()`, får jeg den til at vise det billede af hajen, som jeg har preloaded inde i spillets js.. Jeg bruger dens properties, til at bestemme størrelsen og positionen. 
Under `move()`, skriver jeg at positionen skal blive mindre, med hajens speed. Derudover skriver jeg at der skal komme en ny haj, hvis en haj kommer ud af skærmen. 

`class Food {` består af de samme properties og methods som hajens, da de to skal opføre sig på den samme måde. Inde i spillets js. fil har de dog forskellige funktioner, da hajen resulterer i gameover, og tangen giver en højere score. 

### The characteristics of object-oriented programming

I object-oriented programming har man fokus på programmering med objekter og objekters data frem for funktioner og logikker. Derudover handler det også abstraktion og de processer, når man skal oversætte fysiske objekter til ideen om et objekt. Når man gør dette handler det om at udelade visse detaljer og kontekstuel information uundgåeligt. Det handler om at være i stand til at kunne abstrahere visse detaljer fra et objekt, for at kunne præsentere en konkret model. 
“In other words, objects in OOP are not only about negotiating with the real world as a form of realism and representation, nor about the functions and logic that compose the objects, but the wider relations and “interactions between and with the computational”” (Soon & Cox). Beatrice Fazi and Matthew Fuller fortæller her hvordan abstraktion ikke kun bruges til at oversætte objekter fra den virkelige verden og repræsentere dem, men også til at deltage i verdenen.
Dette er også noget jeg har gjort i min kode, når jeg har bestemt hvad de objekter der indegår i mit spil, har skulle kunne og hvordan de skulle vises. 

### Cultural context

I mit spil har jeg brugt abstraktion til at bestemme hvad mine objekter skulle kunne. Dermed hvordan hajerne skulle bevæge sig, deres størrelse og position. Disse properties og methods kunne jeg have valgt at gøre mere kompliceret, men lod vær da jeg bare gik med at definerer det, som var nødvendigt. En vigtig overvejelse, man skal gøre sig når man taler om abstraktion er, hvordan nogle detaljer (måske vigtige) vil blive ignoreret, når et objekt fra den virkelige verden konverteres til et computerprogram. Det er også noget jeg har gjort brug af i mit program, hvor jeg har ”skåret ind til benet” med objekternes egenskaber, i forhold til hvordan de er i den virkelige verden. 
"With the abstraction of relations, there is an interplay between abstract and concrete reality." (Soon & Cox). Der er dog stadig sammenspil mellem virkeligheden og det abstrakte.


