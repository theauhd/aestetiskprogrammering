# MiniX2 - README
Hej og velkommen til min miniX2

# RUNME link
Smagsprøve på min miniX2, så klik her for at se hvad jeg har lavet juhu

https://theauhd.gitlab.io/aestetiskprogrammering/miniX2/index.html

Her kan du se min kode:
https://gitlab.com/theauhd/aestetiskprogrammering/-/blob/main/miniX2/sketchminix2.js


# README
**Tankerne bag min miniX2**

I min miniX2 har jeg som vi skulle i opgaven lavet to emojis. Selve de emojis jeg har lavet, har jeg lavet med inspiration har de gule emojis som allerede findes, og som folk bruger hver dag.

Noget af det jeg tænkte over da jeg lavede dem, var hvordan vi rent faktisk bruger emojis. Får når vi sender en grine emoji, en græde emoji eller en emoji der er rød af raseri, er det jo ikke sådan vores ansigtsudtryk rent faktisk ser ud. I hvert fald ikke i de fleste tilfælde. Derfor synes jeg det var sjovt at kigge på det her koncept med, at vi kan se ud på en måde, men føle noget helt andet, som folk der ser på en udefra ikke altid kan se. 

I min miniX2 har jeg taget udgangspunkt i det nogle kalder ”resting bitch face”, som man tit godt kan have når man sidder og tænker på alt mellem himmel og jord. Man kan altså have det skidesjovt inde i hovedet, uden at nogle ved hvad der foregår. Dette er bare noget jeg synes var lidt sjovt at have med, når man snakker om emojis, for det er tit på den måde de bliver brugt. Hvor man sidder derhjemme i sofaen skriver med en, der skriver noget sjovt, hvorefter man sender en emoji, der viser hvordan man måske ville reagerer hvis man var sammen med personen, men i virkeligheden sidder man bare uden nogen form for mimik i ansigtet. 

Min miniX2 har nok ikke så stor fokus på hvordan man kan fikse det her store problem, med de minoriteter, der kan føle sig ekskluderet af de her emojis. Dog mere sådan nogle sjove tanker om dem, og måden de bliver brugt på. 

**Hvad har jeg programmeret?**

Øverst har jeg skrevet “How I look daydreaming”. Under har eg lavet en emoji, som ser lidt sur ud, som var meningen at skulle have det her ”resting bitch face”. Som forklaret med teksten, skal det altså forestille ens ansigtsudtryk når man sidder i sine egne tanker og dagdrømmer. 

Jeg har gjort sådan at emojien kan blinke når man trykker på skærmen med musen. For at gøre dette har jeg brugt funktionen `if (mouseIsPressed)`, hvorefter jeg så har slettet øjnene, så det var orange under, så det giver illustrationen af et øjenlåg. Dette var egentlig mest bare for at skabe en smule interaktion og lege lidt med de features. 

I hjørnet har jeg så lavet en firkant, hvor man kan tage musen ind i, og der vil derefter poppe en ny emoji op, samt teksten “How I feel daydreaming”. Dette er altså for at vise hvordan man rent faktisk har det inden i, selvom man måske ikke ser sådan ud.

 Hvis man så trykker med musen inde i den lille firkant, vil den nye emoji også blinke.
Til sidst tilføjede jeg også en lille pil og en tekst ”mouse there”, for at indikere at man kunne placere musen i firkanten, og derefter se en ændring. 

Du kan se meget mere indenunder min sketch, hvor jeg har skrevet over hver kode, hvad jeg har lavet med den.

**Hvad har jeg lært?**

Denne miniX har lært mig meget omkring hvordan man kan lave det man har kodet interaktivt, f.eks. med knapper eller med andre funktioner, som musen har. Det jeg synes er fedt, da det kun ka blive brugbart til de næste opgaver, for at gøre det lidt sjovere. 

Jeg har derudover også lært lidt om hvordan man kan lave tekst og lave det i forskellige teksttyper, hvilket også er ret fedt at vide. Jeg prøvede også at lege lidt med nogle variabler, for at få øjnene til at bevæge sig, hvilket ikke helt lykkedes for mig, men det er noget jeg vil prøve i næste miniX, og se om jeg kan få det til at fungere der, og måske lave noget sejt med dem. 

