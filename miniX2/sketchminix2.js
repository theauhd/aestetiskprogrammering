
function setup() {
  // put setup code here
createCanvas(900, 700);


}
function draw() {
  // put drawing code here
  // baggrunden
background(220,210,255);

//teksten
fill(255,200, 0);
textSize(50);
textFont("Staatliches");
strokeWeight(5);
stroke(255,165,0);
text("HOW I LOOK",370,90);

fill(147,112,219);
textSize(40);
textFont("Sacramento");
strokeWeight(3);
stroke(72, 61,139);
text("daydreaming", 390,120);

//hovedet
fill(255,200, 0);
stroke(255,165,0);
strokeWeight(10);
ellipseMode(CENTER);
ellipse(450,350,400);
//kinder
fill(255,192,203);
noStroke();
ellipse(340,385,50,30);
ellipse(560,385,50,30);

// øjne
fill(255);
stroke(255,165,0);
arc(380,300, 125, 130, 0, PI + QUARTER_PI, CHORD);
arc(520,300, 125, 130, radians(307),radians(180), CHORD);
fill(0);
noStroke();
arc(380,293, 70, 80, 0, PI + QUARTER_PI, OPEN);
arc(520,290, 70, 80, radians(310),radians(180), OPEN);

//mund
stroke(255,165,0);
line(385,455,515,430);

//får den til at blinke når man trykker med musen
if(mouseIsPressed){
  push();
  fill(255,200, 0);
  noStroke();
  
  erase();
  arc(380,300, 125, 130, 0, PI + QUARTER_PI, CHORD);

  erase();
  arc(520,300, 125, 130, radians(307),radians(180), CHORD);

  erase();
  ellipse(515,310,50,60);

  erase();
  ellipse(385,310,50,60);

  pop()
}


//firkanten
fill(255);
stroke(221,160,221);
rect(100,100,100,100);

//pil + tekst til pil
push();
textSize(15);
noStroke();

textFont("Helvetica");
text("mouse there",60,370);

pop();

stroke(147,112,219);
strokeWeight(5);
line(100,250,50,350);
line(70,270,100,250);
line(105,285,100,250);

// en firkant, som skifter farve når mussen er indeni firkanten
//happy face, kommer når musen er inden i firkanten
if (mouseX>100 && mouseX<200 && mouseY>100 && mouseY<200){
background(220,210,255);

//teksten
fill(255,200, 0);
textSize(50);
textFont("Staatliches");
strokeWeight(5);
stroke(255,165,0);
text("HOW I FEEL",370,90);

fill(147,112,219);
textSize(40);
textFont("Sacramento");
strokeWeight(3);
stroke(72, 61,139);
text("daydreaming", 390,120);

//firkanten
fill(255);
stroke(221,160,221);
strokeWeight(10)
rect(100,100,100,100); 

//Det nye hoved
fill(255,200, 0);
stroke(255,165,0);
strokeWeight(10);
ellipseMode(CENTER);
ellipse(450,350,400);

//Øjne
fill(255);
ellipse(515,310,105,120);
ellipse(385,310,105, 120);

//det sorte i øjet
fill(0);
noStroke();
ellipse(515,320,70,80);
ellipse(385,320,70,80);
//det hvide i øjet
fill(255);
ellipse(503,307,10,15);
ellipse(525,335,10,15);

ellipse(373,307,10,15);
ellipse(396,335,10,15);

//munden + tand
fill(240,128,128);
stroke(255,165,0);

arc(446, 410, 175, 150, 0, PI + PI*2, PIE);

fill(255);
noStroke();
rect(446, 415,30,20);

//kinderne
fill(255,192,203);
noStroke();
ellipse(340,385,50,30);
ellipse(560,385,50,30);

  //får den til at blinke
  if(mouseIsPressed){
    push();
    fill(255,200, 0);
    noStroke();
    
    erase();
    ellipse(515,310,105,120);
    ellipse(385,310,105,120);
  
    pop()
  }
}
}

